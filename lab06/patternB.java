/* Andrew Taulane
CSE002 - Lab #6 Pattern B */
import java.util.*; // imports the scanner
class patternB { 
  // Every java program needs a main method
 public static void main(String[] args) {
   Scanner input = new Scanner(System.in); // Declares and initializes the scanner input
   int runs = 0; // Declares and initializes the rest of the variables I'll be using in my loops
   int i = 0;
   int j = 0;
   boolean status = true;
   boolean status2 = true;
  
   while (status) {
    System.out.print ("How long should the pattern be? Enter an integer between 1 and 10:");
    status2 = input.hasNextInt(); // Checks if the user has inputted an integer
     if (status2) { // Will only run if status2 is true, i.e, if the user has entered an integer
       runs = input.nextInt(); 
       if (runs >= 1 && runs <= 10) {
         status = false; // Sets the boolean status to false to break out of the while loop if the integer is between/equals to 1 and 10
       }
       else { // If the user doesn't input an integer between 1 and 10, this statement will run and restart the while loop
         System.out.println ("Please enter an intger between 1 and 10. Restarting...");
         status = true;
       }
     }
     else { // If the user doesn't enter an integer at all, this stat will run and restart the while loop
       input.next();
       System.out.println ("Please enter an integer. Restarting...");
       status = true;
     }
    }

    for (i = 1; i <= runs; runs--) { // First loop: runs as many times as the user specifies, but decreases the number of runs with each run through
      for (j = 1; j <= runs; j++) { // Second loop: runs as many times as the user specifies, but increases the variable j each time
       System.out.print (j + " "); // These two loops combined give us the upside down pyramid effect we want
      }
      System.out.println (""); // Makes a new line for the next line of the pyramid
    }

   input.close(); // Closes the input
 }
}
