/* Andrew Taulane
CSE002 - Lab #6 Pattern C */
import java.util.*; // Imports the scanner
class patternC {
  // Every java program needs a main method
public static void main(String[] args) {
 Scanner input = new Scanner(System.in); // Declaring and intializing the scanner input
 int runs = 0; // Declaring and intializing all the rest of variables I'll use in my while and for loops
 int i = 0; 
 int j = 0;
 int k = 0;
 boolean status = true;
 boolean status2 = true;
 
  while (status) {
  System.out.print ("How long should the pattern be? Enter an integer between 1 and 10:");
  status2 = input.hasNextInt(); // Checks if the user has inputted an integer
   if (status2) {  // Will only run if status2 is true, i.e, if the user has entered an integer
     runs = input.nextInt(); 
     if (runs >= 1 && runs <= 10) {
       status = false; // Sets the boolean status to false to break out of the while loop if the integer is between/equals to 1 and 10
     }
     else { // If the user doesn't input an integer between 1 and 10, this statement will run and restart the while loop
       System.out.println ("Please enter an intger between 1 and 10. Restarting...");
       status = true;
     }
   }
   else { // If the user doesn't enter an integer at all, this stat will run and restart the while loop
     input.next();
     System.out.println ("Please enter an integer. Restarting...");
     status = true;
   }
  }

  for (i = 1; i <= runs; i++) { // First loop: runs as many times as the user specifies, increments by one each time
    for (k = runs; k >= i; k--) { // Second loop: sets k to the amount of user runs and keeps running as long as it's above the variable i. Decrements b/c we need less spaces each time
      System.out.print (" ");
    }
    for (j = i; j > 0; j--) { // Third loop: runs as long as j remains above 0, decreases with each run
     System.out.print (j); // By decreasing the inner loop, we get the counting backwards effect we want
    }
    System.out.println(" "); // Makes a new line for the next line of the pyramid
  }

 input.close(); // Closes the scanner
}
}