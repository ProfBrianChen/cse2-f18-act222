/* Andrew Taulane
CSE002 Hw04 - CrapsSwitch */

import java.util.*;
class CrapsSwitch {
  // Every java program needs a main method
  public static void main(String[] args) {
    Scanner input = new Scanner (System.in); // Decalares and intializes a Scanner 'input'. We'll use this one for string variables
    Scanner input2 = new Scanner (System.in); // // Declares and intializes a Scanner 'input2' We'll use this one for int variables
    System.out.print ("Would you like to randomly roll two dice or state them? Enter 'roll' or 'state':");
    String resp = input.next(); // Records the String 'resp' as 'input'
    
    switch (resp) { // This switch monitors the resp String
      case ("roll"): // This case only activates if the user enter 'roll' for the resp String
      int random = (int) ((Math.random() * 6) + 1); // Intializes and declares a random int 'random' between 1 and 6
      int random2 = (int) ((Math.random() * 6) + 1); // Intitalzies and declares a random int 'random2' between 1 and 6
      int total = random + random2; // Adds the two random dice rolls
      System.out.println("You rolled a " + random + " and a " + random2 + "! You total is " + total + "!"); // Displays the dice and the total
      
      switch (total) {
        case 2: // This case will activate if the two dice add up to two
          System.out.println ("Snake Eyes!");
          break;
        case 3: 
          System.out.println ("Ace Deuce!");
          break;
        case 4:
          switch (random) {
            case 1: case 3: // This case wil activate if the total is 4 and the first dice is either 1 or 3
              System.out.println ("Easy Four!");
              break;
            case 2:  // This case will activate if the total is 4 and the first dice is 2
              System.out.println ("Hard Four!");
              break;
           }
          break; // Breaks prevent leakage of responses
        case 5:
          System.out.println ("Fever Five!");
          break;
        case 6:
          switch (random) {
            case 1: case 2: case 4: case 5:
              System.out.println ("Easy Six!");
              break;
            case 3:
              System.out.println ("Hard Six!");
              break;
           }
          break;
        case 7:
          System.out.println ("Seven Out!");
          break;
        case 8:
          switch (random) {
            case 2: case 3: case 5: case 6:
              System.out.println ("Easy Eight!");
              break;
            case 4:
              System.out.println ("Hard Eight!");
              break;
            }
          break;
        case 9: 
          System.out.println ("Nine!");
          break;
        case 10:
          switch (random) {
            case 4: case 6:
              System.out.println ("Easy Ten!");
              break;
            case 5:
              System.out.println ("Hard Ten!");
              break;
          }
          break;
        case 11:
          System.out.println ("Yo-Leven!");
          break;
        case 12: 
          System.out.println ("Boxcars!");
          break;
      }
      break;
      
      case ("state"): // This case will only activate if the user enters in 'state' for the resp String
        System.out.print ("What is your first dice roll?");
        int d1 = input2.nextInt(); // Declares and intializes the int variable d1
        System.out.print ("What is your second dice roll?");
        int d2 = input.nextInt(); // Declares and intializes the int variable d2
        int total2 = d1 + d2; // Adds d1 and d2 to get the total of the two dice
        
        switch (total2) { // This switch monitors the total2 variable
        case 2: // The case will only activate if the total of the two user-entered dice is 2
          System.out.println ("Snake Eyes!");
          break;
        case 3: 
          System.out.println ("Ace Deuce!");
          break;
        case 4:
          switch (d1) {
            case 1: case 3: // This case will only activate if the total of the two user-entered dice is 4 and the first dice is either 1 or 3
              System.out.println ("Easy Four!");
              break;
            case 2:  // This case will only activate if the total of the two user-entered dice is 4 and the first dice is 3
              System.out.println ("Hard Four!");
              break;
           }
          break; // Breaks prevent data leakage
        case 5:
          System.out.println ("Fever Five!");
          break;
        case 6:
          switch (d1) {
            case 1: case 2: case 4: case 5:
              System.out.println ("Easy Six!");
              break;
            case 3:
              System.out.println ("Hard Six!");
              break;
           }
          break;
        case 7:
          System.out.println ("Seven Out!");
          break;
        case 8:
          switch (d1) {
            case 2: case 3: case 5: case 6:
              System.out.println ("Easy Eight!");
              break;
            case 4:
              System.out.println ("Hard Eight!");
              break;
            }
          break;
        case 9: 
          System.out.println ("Nine!");
          break;
        case 10:
          switch (d1) {
            case 4: case 6:
              System.out.println ("Easy Ten!");
              break;
            case 5:
              System.out.println ("Hard Ten!");
              break;
          }
          break;
        case 11:
          System.out.println ("Yo-Leven!");
          break;
        case 12: 
          System.out.println ("Boxcars!");
          break;
        default:
            System.out.println ("Enter two valid dice rolls");
            break;
            
      }
        break;
        
      default: // If the user doesn't enter 'roll' or 'state' when prompted, this case will run
        System.out.println ("Please enter 'roll' or 'state' for the program to run.");
        break;
        
        
      
      
    }
  
     input.close(); // Closes the Scanner 'input'
     input2.close(); // Closes the Scanner 'input2'
  }
  //Closes Main Method
}