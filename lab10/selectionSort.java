/*Andrew Taulane
CSE002 - Lab#10*/
import java.util.*;
class selectionSort {
  public static void main(String[] args) {
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = selectionSort(myArrayBest);
    System.out.println("");
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		int iterWorst = selectionSort(myArrayWorst);
    System.out.println("");
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}

	public static int selectionSort(int[] list) { 
    System.out.println(Arrays.toString(list));
		int iterations = 0;

		for (int i = 0; i < list.length - 1; i++) {
			iterations++;
			int currentMin = list[i]; 
			int currentMinIndex = i;
			for (int j = i + 1; j < list.length; j++) { 
        if (list[j] < list[i]) {
          currentMin = list[j];
          currentMinIndex = j;
          iterations++;
        }
        else {
          iterations++;
        }
			}
			
      if (currentMinIndex != i) { 
        list[currentMinIndex] = list[i];
        list[i] = currentMin;
        System.out.println("");
        System.out.print(Arrays.toString(list));
			}
		}
		return iterations;
	}
}
