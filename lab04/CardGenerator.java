/* Andrew Taulane
CSE002: Lab 04 */
// This program chooses a random card from a standard deck of 52 playing cards and displays it

import java.util.*;
class CardGenerator{
  // Every java program needs a main method
  public static void main(String[] args) {
    int random = (int) ((Math.random() * 51) + 1); // Creates a double variable between 1 and 52 and casts it as the the int variable random
    
    switch (random) {
      case 1:
        System.out.println ("You picked the Ace of Diamonds."); // Prints if random = 1 
        break; // break; ends the case
      case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: // Prints if random = 2-10
        System.out.println ("You picked the " + random + " of Diamonds"); // Displays the number of the card chosen
        break;
      case 11: 
        System.out.println ("You picked the Jack of Diamonds."); // Prints if random = 11 
        break;
      case 12:
        System.out.println ("You picked the Queen of Diamonds."); // Prints if random = 12
        break;
      case 13:
        System.out.println ("You picked the King of Diamonds."); // Prints if random = 13
        break;
      case 14:
        System.out.println ("You picked the Ace of Clubs.");
        break;
      case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: 
        System.out.println ("You picked the " + (random - 13) + " of Clubs"); // Subtract 13 from random to display the proper number of the card
        break;
      case 24: 
        System.out.println ("You picked the Jack of Clubs.");
      case 25:
        System.out.println ("You picked the Queen of Clubs.");
        break;
      case 26:
        System.out.println ("You picked the King of Clubs."); 
        break;
      case 27:
        System.out.println ("You picked the Ace of Hearts.");
        break;
      case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36:
        System.out.println ("You picked the " + (random - 26) + " of Hearts."); // Subtracts 26 from random to display the proper number of the card
        break;
      case 37: 
        System.out.println ("You picked the Jack of Hearts.");
        break;
      case 38:
        System.out.println ("You picked the Queen of Hearts.");
        break;
      case 39:
        System.out.println ("You picked the King of Hearts.");
        break;
      case 40:
        System.out.println ("You picked the Ace of Spades.");
        break;
      case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49:
        System.out.println ("You picked the " + (random - 39) + " of Spades."); // Subtracts 39 from random to display the proper number of the card
        break;
      case 50:
        System.out.println ("You picked the Jack of Spades.");
        break;
      case 51:
        System.out.println ("You picked the Queen of Spades");
        break;
      case 52:
        System.out.println ("You picked the King of Spades.");
        break;
    }
  } // Ends main method
}