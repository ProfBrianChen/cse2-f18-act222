/* Andrew Taulane
CSE002 - Lab #6 Pattern A */
import java.util.*; // Imports the scanner 
class patternA {
  // Every java program needs a main method
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int runs = 0; // Declaring and intializing all the variables I'll use in my while and for loops
    String temp = "";
    boolean status = true;
    boolean status2 = true;
    
    while (status) {
     System.out.print ("How long should the pattern be? Enter an integer between 1 and 10:");
     status2 = input.hasNextInt(); // Checks if the user has inputted an integer
      if (status2) { // Will only run if status2 is true, i.e, if the user has entered an integer
        runs = input.nextInt();
        if (runs >= 1 && runs <= 10) { 
          status = false; // Sets the boolean status to false to break out of the while loop if the integer is between/equals to 1 and 10
        }
        else { // If the user doesn't input an integer between 1 and 10, this statement will run and restart the while loop
          System.out.println ("Please enter an intger between 1 and 10. Restarting...");
          status = true;
        }
      
      else { // If the user doesn't enter an integer at all, this stat will run and restart the while loop
        input.next();
        System.out.println ("Please enter an integer. Restarting...");
        status = true;
      }
     }

     for (int i = 1; i <= runs; i++) { // For loop the increments by 1 per loop as long as i is less than or equal to the amount of user runs
         temp = temp + i + " "; // Spaces out the ascending numberes
         System.out.println (temp);
     }

    input.close(); // Closes the scanner
  }
}