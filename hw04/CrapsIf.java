/* Andrew Taulane
CSE002 Hw04 - CrapsIf */

import java.util.*;
class CrapsIf {
  // Every java program needs a main method 
  public static void main(String[] args) {
    Scanner input = new Scanner (System.in); // Decalares and intializes a Scanner 'input'. We'll use this one for string variables
    Scanner input2 = new Scanner (System.in); // Declares and intializes a Scanner 'input2' We'll use this one for int variables
    System.out.print ("Would you like to randomly roll two dice or state them? Enter 'roll' or 'state':");
    String resp = input.next(); // Records the String 'resp' as 'input'

    if (resp.equalsIgnoreCase("roll")) { // This whole if loop only activates if the user choses 'roll' as their response
      int random = (int) ((Math.random() * 6) + 1); // Intializes and declares a random int 'random' between 1 and 6
      int random2 = (int) ((Math.random() * 6) + 1); // Intitalzies and declares a random int 'random2' between 1 and 6
      int total = random + random2;
      System.out.println("You rolled a " + random + " and a " + random2 + "! You total is " + total + "!");

        if (total == 2) { // If the total of the two rolls is 2, this if statement will activate
          System.out.println ("Snake Eyes!");
        } 
        else if (total == 3) {
          System.out.println ("Ace Deuce!");
        }
        else if (total == 4) {
          if (random == 2 && random2 == 2) { // If the total is 4 and each die is 2, this if statement will activate
            System.out.println ("Hard Four!");
          }
          else if (random == 1 || random == 3) { // If the total is 4 and the first die is 1 or 3, this if statement will activate
            System.out.println ("Easy Four!");
          }
        }
        else if (total == 5){
          System.out.println ("Fever Five!");
        }
        else if (total == 6) {
          if (random == 1 || random == 5) {
            System.out.println ("Easy Six!");
          }
          else if (random == 2 || random == 4) {
            System.out.println ("Easy Six!");
          }
          else if (random == 3 && random2 == 3) {
          System.out.println ("Hard Six!");
          }
        }
        else if (total == 7) {
          System.out.println ("Seven Out!");
        }
        else if (total == 8) {
          if (random == 4 && random2 == 4) {
            System.out.println ("Hard Eight!");
          }
          else if (random == 2 || random == 6) {
            System.out.println ("Easy Eight!");
          }
          else if (random == 3 || random == 5) {
            System.out.println ("Easy Eight!");
          }
        }
        else if (total == 9) {
          System.out.println ("Nine!");
        }
        else if (total == 10) {
          if (random == 5 && random2 == 5) {
            System.out.println ("Hard Ten!");
          }
          else if (random == 4 || random == 6) {
            System.out.println ("Easy 10!");
          }
        }
        else if (total == 11) {
          System.out.println ("Yo-Leven!");
        }
        else if (total == 12) {
          System.out.println ("Boxcars!");
        }

      }
    
    else if (resp.equalsIgnoreCase("state")) { // This else if statement will only activate if the user chooses 'state' as their response
      System.out.print ("What is your first dice roll?");
      int d1 = input2.nextInt(); // Stores the int variable d1 in the scanner 'input2'
      System.out.print ("What is your second dice roll?");
      int d2 = input2.nextInt(); //Stores the int variable d2 in the scanner 'input2'
      int total2 = d1 + d2; // Adds the the two dice rolls

      if (total2 == 2) { // This if statement will only activate when the total of the two inputed dice rolls is 2
          System.out.println ("Snake Eyes!");
        } 
        else if (total2 == 3) {
          System.out.println ("Ace Deuce!");
        }
        else if (total2 == 4) {
          if (d1 == 2 && d2 == 2) { // This if statement will only activate if the total of the two rolls if 4 and each dice is 2
            System.out.println ("Hard Four!");
          }
          else if (d1 == 1 || d1 == 3) { // This else if statement will only activate if the total is 4 and the first die is either 1 or 3
            System.out.println ("Easy Four!");
          }
        }
        else if (total2 == 5){
          System.out.println ("Fever Five!");
        }
        else if (total2 == 6) {
          if (d1 == 1 || d1 == 5) {
            System.out.println ("Easy Six!");
          }
          else if (d1 == 2 || d1 == 4) {
            System.out.println ("Easy Six!");
          }
          else if (d1 == 3 && d2 == 3) {
          System.out.println ("Hard Six!");
          }
        }
        else if (total2 == 7) {
          System.out.println ("Seven Out!");
        }
        else if (total2 == 8) {
          if (d1 == 4 && d2 == 4) {
            System.out.println ("Hard Eight!");
          }
          else if (d1 == 2 || d1 == 6) {
            System.out.println ("Easy Eight!");
          }
          else if (d1 == 3 || d1 == 5) {
            System.out.println ("Easy Eight!");
          }
        }
        else if (total2 == 9) {
          System.out.println ("Nine!");
        }
        else if (total2 == 10) {
          if (d1 == 5 && d2 == 5) {
            System.out.println ("Hard Ten!");
          }
          else if (d1 == 4 || d1 == 6) {
            System.out.println ("Easy 10!");
          }
        }
        else if (total2 == 11) {
          System.out.println ("Yo-Leven!");
        }
        else if (total2 == 12) {
          System.out.println ("Boxcars!");
        }
        else {
          System.out.println ("Your two dice were not in the range of possible values. Try again."); //Accounts for non-ints or numbers above 12
        }

    }
    else {
      System.out.println ("Please enter 'roll' or 'state'. Restart the program because I couldn't be bothered to program a loop");
      // If the user doesn't enter 'roll' or 'state', this else statement will activate
    }                                                               
    
    
    input.close(); // Closes the scanner 'input'
    input2.close();  // Closes the scanner 'input2'
  }
  // Closes main method
}