/* Andrew Taulane
CSE002 - Lab 9*/
import java.util.*;
public class arrayInverter{
  // EVery java program needs a main method
  public static void main(String[] args) { 
    int[] array0 = new int[] {1, 2, 3, 5, 7, 9, 10, 6}; // Declares and intializes, and allocates our first array
    int[] array1 = copy(array0); // Creates two copies of the first array by passing array0 into the copy method
    int[] array2 = copy(array0);
    inverter(array0); // Passes array0 to the inverter method
    inverter2(array1); // Passes array1 to the inverter2 method
    int[] array3 = inverter2(array2); // Asigns the array output of passing array2 to the inverter2 method to the array "array3"
    print(array0); // We print out arrays 0, 1, and 3 with the print method
    System.out.println(""); // New line or aesthetic purposes
    print(array1);
    System.out.println("");
    print(array3);
 } // Closes main method

 public static int[] copy(int[] array) { // copy method. Takes input int array "array" and returns and integer array for output
   int[] arrayz = new int[array.length]; // Declares and intializes a new array, "arrayz" with the length of the inputted array
   for (int i = 0; i < array.length; i++) { // For loop that runs as long as its less than the length of the input array
    arrayz[i] = array[i]; // Assigns the value of arrayz at index i to the value of array at index i, effectively making a copy
   }
   return arrayz; // Returns arrayz
 } // Closes copy method

 public static void inverter(int[] array) { // inverter method. Takes int array "array" as input and returns nothing
   int j = 0; // Declares and intializes our temp value, j
   for (int i = 0; i < array.length/2; i++) { // For loop the runs for half of the length of the input array b/c we only need to make 4 swaps to invert the array
     j = array[i]; // Assigns the value of j to the value of array at index i
     array[i] = array[array.length-1-i]; // Assigns the value of array at index i to value of array at the index of the length of the array - 1 - i
     array[array.length-1-i] = j; // Assigns the value of array at this decreasing index value to j
   }
 } // Closes inverter method

 public static int[] inverter2(int[] array) { // inverter2 method. Takes an int array "array" as input and returns an int array
   int[] arrayx = copy(array); // Declares and intializes an array "arrayx" as the copy of the input array with the copy method
   inverter(arrayx); // Inverts arrayx using the inverter method
   return (arrayx); // Returns arrayx
 } // Closes inverter2 method

 public static void print(int[] array) { // print method. Prints out the input array using a for loop
   for (int i = 0; i < array.length; i++) {
     System.out.print(array[i] + " ");
   } // Closes print method
 }

} // Closes program