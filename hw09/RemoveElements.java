/* Andrew Taulane
CSE002 - Homework 9 - Program 2*/
import java.util.*; // Imports the wildcard
public class RemoveElements { 
  public static void main(String [] arg){ // Every java program needs a main method
	Scanner scan = new Scanner(System.in); // Imports the scanner scan
  int num[] = new int[10]; // Declaring the variables we'll be using in the program
  int newArray1[];
  int newArray2[];
  int index,target;
  String answer="";
    do { // Do-while loop. This runs at least once
      System.out.print("Random input 10 ints [0-9] ");
      num = randomInput(num); // Sends num to the randomInput method
      String out = "The original array is: ";
      out += listArray(num); // Adds the output of sending num to listArray method to the string out
      System.out.println(out); // Prints string out
  
      System.out.print("Enter the index ");
      index = scan.nextInt(); // stores the user's input as int index
      newArray1 = delete(num,index); // Sends num and index to the delete method and stores the output as newArray1
      String out1 = "The output array is ";
      out1 += listArray(newArray1); // Converts to string
      System.out.println(out1); // Prints string out1
  
      System.out.print("Enter the target value ");
      target = scan.nextInt(); // store the user's input as int target
      newArray2 = remove(num,target); // Sends num and target to the remove method and stores the output as newArray2
      String out2="The output array is ";
      out2 += listArray(newArray2); // Converts to string
      System.out.println(out2); // Prints string out2
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
      } while (answer.equals("Y") || answer.equals("y")); // As long as the user enters "Y" or "y" the loop will continue
        } // Ends main method
  
    public static String listArray(int num[]){ // listArray method. Accepts int array num as an argument and returns a string
    String out = "{";
    for (int j = 0; j < num.length; j++) { // For loop that runs as long as j is less than the kength of num 
      if (j > 0) {
        out += ", "; 
      }
      out += num[j]; // Adds the value of num at index j to the string out
    }
    out += "} ";
    return out; // Returns the string out
    } // Closes the listArray method

    public static int[] randomInput(int[] num) { // randomInput method. Accepts int array num as an arugment and returns an int array
      Random rand = new Random(); // Declares a new random rand
      for (int i = 0; i < num.length; i++) { // For loop that runs as long as it's less than the less length of num
        int random = rand.nextInt(10); // Sets int random to a random value between 0 and 9
        num[i] = random; // Sets the value of num at index i to the random value
      }
      return num; // Returns num
    } // Closes the randomInput method

    public static int[] delete(int[] list, int pos) { // delete method. Accepts an int array and int as arguments
      if (pos <= 9 && pos >= 0) { // If int pos is less than or equal to 9 and greater than or equal to 0... 
        int[] newArray1 = new int[(list.length - 1)]; // Intialize int array newArray1 to the length of list - 1 
        for (int j = 0; j < list.length; j++) { // For loop that runs as long as it less than the length of array list
          if (j == pos) { // if int j is equal to pos...
            for (int i = pos; i < newArray1.length; i++) { // Create another for loop, this time running the length of newArray1
              newArray1[i] = list[i+1]; // Set the value of newArray1 at index i to the value of list at index i + 1
            }
            break; // Break out of this innermost for loop
          }
          else { // Otherwise...
            newArray1[j] = list[j]; // Copy the values of list to newArray1
          }
        }  
        return newArray1; // Returns the int array newArray1
      }
      else { // Otherwise...
        System.out.println ("ERROR. Index is not valid."); // Print an error and return the unedited list
        return list;
      }
    } // Closes the delete method

    public static int[] remove(int[] list, int target) { // remove method. Accepts int array list and int target as arguments and returns an int array
      boolean status = true; // Declares and initializes boolean status as true
      int count = 0; // Declares and initializes int count as 0
      for (int i = 0; i < list.length; i++) { // For loop that runs as long as it is less than the length of list
        if (list[i] == target) { // If the value of list at index i is int target...
          status = false; // set status to false and increment counter by 1
          count++;
        }
      }
      if (status) { // If status is true...
        System.out.println ("ERROR. Target number " + target + " not found."); // Print an error and return the unedited list
        return list;
      }
      else { // Otherwise...
        System.out.println (target + " has been found."); 
        int targetPos = 0; // Declaress and intiailizes the int targetPos as 0
        int[] newArray2 = new int[(list.length - count)]; // Initializes the array newArray2 with the length of list - the value of count
        for (int j = 0; j < list.length; j++) { // For loop that runs as long as it less than the length of list
          if (list[j] != target) { // If the value of list at index j is target
            newArray2[targetPos++] = list[j]; // Set the the value of newArray2 at the index of the incremented value of targetPos to the value of list at index j
          }
        }
        return newArray2; // return int array newArray2
      }
    } // Closes the remove method
  } // Closes the program
