/* Andrew Taulane
CSE002 - Lab #6 Pattern D */
import java.util.*;  // Imports the scanner
class patternD {
  //Every java program needs a main method
 public static void main(String[] args) {
   Scanner input = new Scanner(System.in); // Declares and intializes the scanner input
   int runs = 0; // Declaring and intializing the rest of the variables I plan on using
   int i = 0;
   int j = 0;
   boolean status = true;
   boolean status2 = true;
  
   while (status) {
    System.out.print ("How long should the pattern be? Enter an integer between 1 and 10:");
    status2 = input.hasNextInt();  // Checks if the user has inputted an integer
     if (status2) { // Will only run if status2 is true, i.e, if the user has entered an integer
       runs = input.nextInt();
       if (runs >= 1 && runs <= 10) {
         status = false;  // Sets the boolean status to false to break out of the while loop if the integer is between/equals to 1 and 10
       }
       else { // If the user doesn't input an integer between 1 and 10, this statement will run and restart the while loop
         System.out.println ("Please enter an intger between 1 and 10. Restarting...");
         status = true;
       }
     }
     else { // If the user doesn't input an integer at all, this statement will run and restart the while loop
       input.next();
       System.out.println ("Please enter an integer. Restarting...");
       status = true;
     }
    }

    for (i = 1; i <= runs; runs--) { // First loop: runs as long as 1 is less than the user specifies amount of loops, but decreases this number by 1 every loop
      for (j = runs; j >= i; j--) { // Second loop: sets j to the amount of runs; this loop will run as long as j is greater than/equal to i, decreases j by 1
       System.out.print (j + " "); // The combination of these two loops creates the pyramid effect we want
      }
      System.out.println ("");
    }

   input.close(); // Closes the scanner
 }
}