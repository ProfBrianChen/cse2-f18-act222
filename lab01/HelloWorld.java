/* Andrew Taulane
CSE02 Lab01: Hello World! */

public class HelloWorld {
  public static void main (String args[]) {
    System.out.println ("Hello World!");
    // Prints "Hello World!" to terminal
  }
}
