/* Andrew Taulane
CSE002 Hw01 */

public class WelcomeClass {
  public static void main (String[] args) {
    System.out.println ("-----------");
    System.out.println ("| WELCOME |");
    System.out.println ("-----------");
    System.out.println ("  ^  ^  ^  ^  ^  ^");
    System.out.println (" / \\/ \\/ \\/ \\/ \\/ \\ "); // Double backslash to indicate that the backslash is not an escape sequence
    System.out.println ("<-A--C--T--2--2--2->");
    System.out.println (" \\ /\\ /\\ /\\ /\\ /\\ / "); 
    System.out.println ("  v  v  v  v  v  v");
                        
  }
}