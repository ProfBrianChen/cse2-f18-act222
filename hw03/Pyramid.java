/* Andrew Taulane
CSE002: Homework 3 - Program 2 */

import java.util.*;
public class Pyramid {
  // Every java program needs a main method 
 public static void main(String[] args) {
   Scanner input = new Scanner (System.in); // Initializing and delcaring a scanner named "input"
   System.out.print ("Enter the square side of the pyramid in inches: ");
   double base = input.nextDouble(); // Initializing and declaring a double value "base" that is stored as the next double in the input scanner 
   double base2 = base * base; // Initialzes and declares a double "base2" which is the same thing as "base^2"

   System.out.print ("Enter the height of the pyramid in inches: ");
   double height = input.nextDouble(); // initializes and declares a double value "height" that is stored as the next double in the input scanner

   double volume = (base2 * height) / 3.0; // Calculates the volume of the square pyramid by multiplying the length by width (Which are the same in this case) by height, then dividing it by 3

   System.out.println ("The volume of the pyramid is " + volume + " cubic inches.");
   
   input.close(); // Closes the input scanner
 }
}