/* Andrew Taulane
CSE002 lab 03 */

import java.util.*;
public class Check {
  // Main method required for every java program
  public static void main (String[] args) {
    Scanner input = new Scanner (System.in); // Initalizes and declares the scanner "input"
    
    System.out.print ("Enter the total check in the form xx.xx");
    double check = input.nextDouble(); // Stores the next reponse as the double "check"
    
    System.out.print ("Enter the percentage of said check that you wish to pay in the form xx");
    double tipPercent = input.nextDouble(); // Stores the next reponse as the double "tipPercent"
    tipPercent /= 100; // Since tipPercent was entered as double that was basically an int, this divides it by 100, and sets it as a decimal
    
    System.out.print ("How many ways do you want to split the check?");
    int split = input.nextInt(); // Stores the next reponse as the int "split"
    
    double totalCheck = check + (tipPercent * check); // Calculates the total cost of the check by adding the check price to the final tip for the check
                                                      // (which is just the decimal tip pecent multiplied by the check amount) 
    double perPerson = totalCheck / (double) split; // Calculates the cost per person by dividing the total check cost by the number of times they wish to split
    
    int dollars = (int) perPerson; // Creates the int variable dollars, which is just an int casted version of "perPerson"
    int dimes = (int) (perPerson * 10) % 10; // Creates an int variable dimes, which stores the remainder of an int version of perPerson * 10 , then / 10
    int pennies = (int) (perPerson * 100) % 10; // Creates an int variable pennies, whch stores the remainder of int version of perPerson * 100, then / 10
    
    // These last three steps are done so that we don't have to display a potentially large double or truncate an answer when we need to be exact
    
    System.out.println ("Each person will pay $" + dollars + "." + dimes + pennies + " dollars."); // Displays the final price each person should pay
    
  } // End of main method
} // End of program