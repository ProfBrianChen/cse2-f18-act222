/* Andrew Taulane
CSE002 - Lab #8 */
class arrayCount {
  // Every java program needs a main method
public static void main(String[] args) {
 int[] array1 = new int[99]; // Declares and intiializes an array named array1 that's 100 values long (0-99)
 int[] array2 = new int[99]; // Declares and initializes an array named array2 that's 100 values long (0-99)
 int temp = 0; // Declares a variable an integer named temp

 System.out.print("Array1 contains "); 
 for (int i = 0; i < array1.length; i++) { // For loop that runs 100 times, incrementing sentinel variable i by 1 each loop
   array1[i] = (int) ((Math.random() * 99) + 0); // Allocates the i location of array1. Sets it to a number between 0 and 99 
   System.out.print(array1[i] + " "); // Prints out the value stored in the array at location i
 } // Closes the for loop
 System.out.println(""); // Creates a new line for aesthetic reasons
 
 for (int j = 0; j < array1.length; j++) { // For loop that runs as long as it is less than the length of the first array
   temp = array1[j]; // Assigns temp to the the value of array1 at location j
   array2[temp]++; // Increments the value of array2 at location temp by 1
 } // Closes the for loop
 
 for (int k = 0; k < array2.length; k++) { // For loop that runs as long as it less than the length of array2
   if (array2[k] > 0 && array2[k] == 1) { // If the value of array2 at location k is greater than zero and equal to 1...
     System.out.println(k + " occurs " + array2[k] + " time."); // Print out the occurances of k
   }
   else if(array2[k] >= 2) { // If the value of array2 at location k is greater than or equal to 2
     System.out.println(k + " occurs " + array2[k] + " times."); // Print out the occurances of k
   }
 } // Closes the for loop
 
  } // CLoses the main method
} // Closes the class
