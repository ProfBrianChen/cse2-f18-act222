/* Andrew Taulane
CSE002 Homework 3 - Program 1*/

import java.util.*;
public class Convert {
  // Every java program needs a main method
 public static void main(String[] args) {
   Scanner input =  new Scanner (System.in); // Initializing and declaring a scanner named "input"
   System.out.print ("Enter the affected area in acres: ");
   double acres = input.nextDouble(); // Declaring a double "acres" that is stored as the next double in the input scanner
   System.out.print ("Enter the rainfall in the affected area in inches: ");
   double inches = input.nextDouble(); // Declaring a double "inches" that is stored as the next double in the input scanner

   double feet = inches * 12.0; // There are 12 inches in a foot
   double acreFoot = feet * acres; // An acre-foot is found by multiplying the feet by the acres affected
   double cubicFeet = acreFoot * 43560; // There are 43560 acre feet in a cubic foot
   double cubicMiles = cubicFeet * (6.79357 * Math.pow(10,12)); // There are 6.79357e-12 (Indicated by the Math.pow function) cubic feet in a cubic mile
   
   System.out.println (cubicMiles); // Prints the cubic miles of rainfall based on acres affected and the average rainfall in inches


   input.close(); // Closes the input to prevent leakage
 }
}
