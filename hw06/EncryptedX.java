/* Andrew Taulane
CSE002 Homework #6 */
import java.util.*; // Allows us to create Scanners to accept user input
class EncryptedX {
  // Every java program needs a main method
  public static void main(String[] args) {
    int i = 0; // These first two variables will be used in our for loops to print the asterisks
    int j = 0;
    int size = 0; // This variable specifies how big our "square" will be
    boolean status = true; // These two booleanss will be used to check if the input is valid
    boolean status2 = true;
    Scanner input = new Scanner (System.in); // Declares and initializes scanner "input"
    
    while (status) {
    System.out.print ("How long should the pattern be? Enter an integer between 1 and 100:");
    status2 = input.hasNextInt(); // Checks if the variable being entered is an integer
     if (status2) { 
       size = input.nextInt(); // variable size is what the user's input is stored as
       if (size >= 1 && size <= 100) {
         status = false; // If the conditions are met, status is set to false, exiting the loop
       }
       else {
         System.out.println ("Please enter an intger between 1 and 100. Restarting...");
         status = true; // Since the variable is not between 1 and 100 when this statement runs, the loop restarts
       }
     }
     else {
       input.next(); // Clears the scanner "input" so a new input can be added
       System.out.println ("Please enter an integer. Restarting...");
       status = true; // Since the variable entered is not an integer if this statment runs, status is set to true and the loop restarts
     }
    }

    for (i = 0; i < size; i++) { // For loop 1: Runs as long as var i is less than var size, and increments i by 1 each loop
      for (j = 0; j < size; j++) { // For loop 2: Runs as longas var j is less than var size and increments j by 1 each loop. This gives us the "square" shape needed
        if (i == j) { // When the variables i and j are equivalent... 
          System.out.print (" "); // A space is printed
        }
        else if (j == (size - i) - 1) { // When j equals i less than the size variable - 1...
          System.out.print (" "); // A space is printed
        }
        else { // Otherwise...
         System.out.print("*"); // An asterisk is printed
        }
      }
     System.out.println (""); // Creates a new line for each loop of the program
    }

   input.close(); // Close the input
   } // Closes the main method
  }
