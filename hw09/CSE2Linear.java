/* Andrew Taulane
CSE002 - Homework 9 - Program 1*/
import java.util.*; // Imports the wildcard. In this case, we'll use it for a scanner
public class CSE2Linear{
 public static void main(String[] args) { // Every java program needs a main method
   int[] array1 = new int[15]; // Declares and initializes a 15 integer length array called array1
   Scanner input = new Scanner(System.in); // Declares and initializes the scanner input 

   System.out.println ("Enter 15 ascending ints for final grades in CSE2: ");
     for (int i = 0; i < 15; i++) { // for loop that runs 15 times, for each element of the array
       boolean status = true; // Declares our two boolean variables, status and and status2 and sets them to true
       boolean status2 = true; 
       while(status) { // While status is true...
        System.out.print("Enter element " + (i+1) + ":");
        status2 = input.hasNextInt(); // Checks if the value of input is an integer
        if (status2 ==  true) { // If it is...
          int a = input.nextInt(); // Takes user input and stores it in scanner input
          if (a < 0 || a > 100) { // If the value of the user input is less than 0 or greater than 100...
            System.out.println("ERROR. Please enter an integer between 0 and 100."); // Print an error
          }
          else { // Otherwise...
            array1[i] = a; // Store the value of the user input in array1 index i
            if (i == 0) { // If i is 0. This is made a separate case because we don't have to check for 0 in our ascending order check
              status = false; // Set status to false, exiting the while loop
            }
            else { // Otherwise...
              if (array1[i] < array1[i-1]) { // If the value of the array at index i is less than the value of the array at the index before i...
                System.out.println("ERROR. Please enter the integers in ascending order."); // Print an error
                status = true; // Set status to true so the loop continues until the user enters a correct value
              }
              else { // Otherwise...
                status = false; // Set status to false, exiting the while loop
              }
            }
          }
        }
        else if (status2 == false) { // If it isn't...
          input.next(); // Clear the user input
          System.out.println("ERROR. Please enter an integer."); // Print an error
          status = true; // Set status to true so the loop continues
        }
       } // Closes the while loop
     } // Closes for loop
   
   System.out.print("Enter a grade to be searched for: ");
   int grade = input.nextInt(); // Stores the user's input as int grade
   binary(grade, array1); // Sends grade and array1 to the binary method
   System.out.println("Scrambling...");
   scramble(array1); // Sends array1 to scramble method
   print(array1); // Sends array1 to the print method
   System.out.println(""); // New line for aesthetic reasons
   System.out.print("Enter another grade to be searched for: ");
   int grade2  = input.nextInt(); // Stores the user's as int grade2
   linear(grade2, array1); // Sends grade2 and array1 to the linear method
 }

public static void print(int[] array1) { // Void method print. Accepts an integer array as the argument 
  for (int i = 0; i < array1.length; i++) {
    System.out.print(array1[i] + " "); // Prints out the array in the argument using a for loop
  }
} // Closes print method

public static void scramble(int[] array1) { // Void method scramble. Accepts an integer array as the argument
  int temp = 0; // Declares int temp
  for (int i = 0; i < 20; i++) { // For loop that runs 20 times
    Random rand = new Random(); // Declares a new random called rand
    int random = rand.nextInt(array1.length); // Sets the int random to a random generated value between 0 and the length of array1 - 
    temp = array1[0]; // Sets temp to value of array1 at the 0th index
    array1[0] = array1[random]; // Sets the value of array1 at the 0th index to the value of array1 at a random index
    array1[random] = temp; // Sets the value of array1 at a random index to the value of temp, scrambling the array
  }
} // Closes scramble method

public static void binary(int grade, int[] array1) { // Void method binary. Accepts an int and int array as arguments
  int count = 1; // Sets int count to 1
  boolean status3 = false; // Sets boolean status3 to false
  int low = 0; // Sets int low to 0
  int high = (array1.length - 1); // Sets int high to the length of array1 - 1
  while (high >= low) { // While int high is greater than or equal to int low
    int mid = (high + low) / 2; // Declares and intializes int mid to the value of high + low divided by 2. This is our middle value for binary search
    if (grade < array1[mid]) { // If the grade we're searching for is less than the value of array1 at the middle index...
      high = mid - 1; // Set high to mid -1
      count++; // Increment count by 1
    } 
    else if (grade == array1[mid]) { // If grade is equal to the value of array1 at mid index...
      System.out.println (grade + " was found after " + count + " iterations."); 
      status3 = true; // Set status3 to true
      break; // Break out of the while loop
    }
    else if (grade > array1[mid]) { // If the grade is higher than the middle...
      low = mid + 1; // Set low to mid + 1
      count++; // Increment count by 1
    }
  }
  if (status3 == false) { // If status is still false...
    System.out.println(grade + " was not found after " + count + " iterations."); // We didn't find the grade
  }
} // Closes binary method

public static void linear(int grade2, int[] array1) { // Void method linear. Accepts an int and int array as arguments
  boolean status4 = false; // Declares and initializes boolean status4 as false
  int count = 15; // Sets int count to 15
  for (count = 0; count < array1.length; count++) { // For loop that runs for the length of the array
    if (grade2 == array1[count]) { // If grade2 is found at the count index...
      System.out.println(grade2 + " was found after " + (count + 1) + " iterations.");
      status4 = true; // status 4 is set to true
      break; // Breaks out of the for loop 
    }
  }
  if (status4 == false) { // If status4 is still false...
    System.out.println(grade2 + " was not found after " + count + " iterations."); // We couldn't find the grade
  }  
} // Closes linear method
} // Ends program