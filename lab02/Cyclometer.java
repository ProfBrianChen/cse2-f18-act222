/* Andrew Taulane
lab02: Cyclometer */

// This program displays the number of minutes and wheel rotations for each given bike trip, as well as the distance in miles of each trip and the trips combined

public class Cyclometer {
  // Every java program needs a main method
  public static void main (String[] args) {
    int secs1 = 480; //Time in seconds of the first trip
    int secs2 = 3220; // Time in seconds of the second trip
    int counts1 = 1561; //Number of wheel rotations for the first trip
    int counts2 = 9037; //Number of wheel rotations for the second trip
    
    double wheelDiameter = 27.0; //The diameter of the wheel in inches
  	double pi = 3.14159; // Pi to 5 digits
  	int feetPerMile = 5280; // The number of feet per mile
  	int inchesPerFoot = 12; // The number of inches per foot
  	int secondsPerMinute = 60; // The number of seconds per minute
	  double distanceTrip1, distanceTrip2,totalDistance; //Declaring the double variables of the distance of the first and second trips, as well as the total diatance
    
    System.out.println ("Trip #1 took " + ((double) secs1/secondsPerMinute) + " minutes, and had " + counts1 + " counts."); // Gives the minutes of trip 1
                                                                                                                            // By dividing secs by 60
    System.out.println ("Trip #2 took " + ((double) secs2/secondsPerMinute) + " minutes, and had " + counts2 + " counts."); // Gives the minutes of trip 2
                                                                                                                            // By dividing secs by 60
    
    distanceTrip1 = (double) counts1 * wheelDiameter * pi; // Calculates the disatance of the first trip in inches by
                                                           // multiplying the counts of the wheel's rotations by the diameter of the wheel and pi
    distanceTrip2 = (double) counts2 * wheelDiameter * pi; // Calculates the distance of the seconds trip in iches by 
                                                           // multiplying the counts of the wheel's rotations by the diameter of the wheel and pi
    
    distanceTrip1 /= (double) inchesPerFoot * (double) feetPerMile; // Calculates the miles of trip 1 by converting it into feet and then miles 
    distanceTrip2 /= (double) inchesPerFoot * (double) feetPerMile; // Calculates the miles of trip 2 by converting it into feet and then miles
    
    System.out.println ("Trip #1 was " + distanceTrip1 + " miles.");
    System.out.println ("Trip #2 was " + distanceTrip2 + " miles.");
    System.out.println ("The total of the two trips was " + (distanceTrip1 + distanceTrip2) + " miles."); // The total distance is the first trip + the seconds trip
    
    
  } // End method
} // End class 