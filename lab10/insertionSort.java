/*Andrew Taulane
CSE002 - Lab#10*/
import java.util.*;
class insertionSort {
  public static void main(String[] args) {
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = insertionSort(myArrayBest);
    System.out.println("");
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
	  int iterWorst = insertionSort(myArrayWorst);
    System.out.println("");
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
	public static int insertionSort(int[] list) {
		System.out.println(Arrays.toString(list));
		int iterations = 0;
    boolean print = true;
		for (int i = 1; i < list.length; i++) {
			iterations++;
			for(int j = i ; j > 0 ; j--){
				if(list[j] < list[j-1]){
          int temp = list[j];
          list[j] = list[j-1];
          list[j-1] = temp;
          iterations++;
				}
				else {
          if (list[j-1] < list[j]) {
            print = false;
            break;
          }
				}				
			}
      if (print == true) {
        System.out.println(Arrays.toString(list));
      }
		}
		return iterations;
	}

}