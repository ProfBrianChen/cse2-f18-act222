import java.util.*;
class ClassInfo {
  // Every java program needs a main method
  public static void main(String[] args) {
    Scanner input = new Scanner (System.in); // Declares and intitializes a scanner called "input". This one will be for numbers 
    Scanner input2 = new Scanner (System.in); // Declares and intitializes a scanner called "input2". This one will be for Strings
    int num = 0; // Declares and initializes an int called num at 0
    String dep = " "; // Declares and initializes a string called dep and sets it to null
    int week = 0; // Declares and initializes an int called week at 0
    String time = " "; // Declares and initializes a string called time and sets it to null
    String name = " "; // Declares and initializes a string called name and sets it to null
    int size = 0; // Declares and initializes an int called size and sets it 0
    boolean status =  true; // I declare and initiale 12 booleans here to serve as breaks for my while and if statements
    boolean status2 = true;
    boolean status3 = true;
    boolean status4 = true;
    boolean status5 = true;
    boolean status6 = true;
    boolean status7 = true;
    boolean status8 = true;
    boolean status9 = true;
    boolean status10 = true;
    boolean status11 = true;
    boolean status12 = true;

    while (status) {
      System.out.print ("What is the course number?"); // This statement will always be run until the user inputs an int (see lines 30-31)
      status2 = input.hasNextInt(); // Checks if the user has inputted an int
       if (status2) {
         num = input.nextInt(); 
         status = false; // Closes this while loop
       }
       else {
         input.next(); // Clears the past input if it's not an int
         System.out.println ("Please enter an integer.");
         status = true; // Continues the loop
       }
    }

    while (status3) {
      System.out.print ("What is the department name?"); // This statement will always run until the user enters in a string (see lines 44-45)
      status4 = input2.hasNextLine(); // Checks if the user has inputted a string
      if (status4) {
        dep = input2.nextLine(); // If the user has inputted a string, dep is set to input2
        status3 = false; // Closes the while loop
      }
      else {
        input2.next(); // Clears the past input
        System.out.println ("Enter a string.");
        status3 = true; // Continues the loop
      }
    }

    while (status5) {
      System.out.print ("How many times per week do you meet?");
      status6 = input.hasNextInt();
      if (status6) {
        week = input.nextInt();
        status5 = false;
      }
      else {
        input.next();
        System.out.println ("Please enter an integer.");
        status5 = true;
      } 
    }

    while (status7) {
      System.out.print ("What time does the class start?");
      status8 = input2.hasNextLine();
      if (status8) {
        time = input2.nextLine();
        status7 = false;
      }
      else {
        input2.next();
        System.out.println ("Please enter a string.");
        status7 = true;
      }
    }

    while (status9) {
      System.out.print ("What is the professor's name?");
      status10 = input2.hasNextLine();
      if (status10) {
        name = input2.nextLine();
        status9 = false;
      }
      else {
        input2.next();
        System.out.println ("Please enter a string.");
        status9 = true;
      }
    }

    while (status11) {
      System.out.print ("How many students are in the class?");
      status12 = input.hasNextInt();
      if (status12) {
        size = input.nextInt();
        status11 = false;
      }
      else {
        input.next();
        System.out.println ("Please enter an integer.");
        status11 = true;
      }
    }
    System.out.println(num); // Prints out the user's response, assuming it was the right type
    System.out.println(dep);
    System.out.println(week);
    System.out.println(time);
    System.out.println(name);
    System.out.println(size);
    input.close(); // Closes the scanner to prevent leakage
    input2.close();
  }
}