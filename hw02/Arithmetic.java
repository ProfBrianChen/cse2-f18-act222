/* Andrew Taulane
CSE002 */

public class Arithmetic {
  public static void main (String[] args) {
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //Cost per belt
    double paSalesTax = 0.06; //The tax rate
    
    double totalPricePants = ( (double) numPants * pantsPrice); // Calculates the total price of the pants w/o tax
    double taxPants = (totalPricePants * paSalesTax); // Calculates the tax to be added to the pants price
    
    double totalPriceShirts = ((double) numShirts * shirtPrice); // Calculates the total price of shirts w/o tax
    double taxShirts = (totalPriceShirts * paSalesTax); // Calculates the tax to be added to the shirts price
    
    double totalPriceBelts = ((double) numBelts * beltPrice); // Calculates the total price of the belts w/o tax
    double taxBelts = (totalPriceBelts * paSalesTax); // Calculates the tax to be added to the belts price
    
    double uglyPrice = totalPriceBelts + totalPriceShirts + totalPricePants; //Calculates the total unrounded price of all the items w/o tax 
    double uglyTax = taxBelts + taxShirts + taxPants; // Calculates the total unrounded tax on all the items
    
    double totalPrice = (((int) uglyPrice * 100) / 100.0); // Rounds the total price to an incorrect but more visually appealing number
    double totalTax = (((int) uglyTax * 100 ) / 100.0); // Rounds the total tax to an incorrect but more visually appealing number
    
    double finalPrice = totalPrice +  totalTax; // Calculates the final price of all the items, plus tax
    
    
    System.out.println ("The total price of the pants without tax is " + totalPricePants + " dollars."); // Displays the total price of the pants
    System.out.println ("The total tax on pants is " + taxPants + " dollars."); // Displays the total tax on the pants
    System.out.println ("The total price of the shirts without tax is " + totalPriceShirts + " dollars."); // Displays the total price of the shirts
    System.out.println ("The total tax on shirts is " + taxShirts + " dollars."); // Displays the total tax on the shirts
    System.out.println ("The total price of the belts without tax is " + totalPriceBelts + " dollars."); // Displays the total price of the belt
    System.out.println ("The total tax on belts is " + taxBelts+ " dollars."); // Displays the total tax on the belt
    System.out.println ("The total price without tax is " + totalPrice + " dollars."); // Displays the total price of the items w/o tax
    System.out.println ("The total tax on the items is " + totalTax + " dollars."); // Displays the total tax on the items
    System.out.println ("The final price of the purchase is " + finalPrice + " dollars."); // Displays the final price
    
    
  }
}
