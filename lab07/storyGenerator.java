import java.util.*;
class storyGenerator {
 public static void main(String[] args) {
   para(); // Calls the para method. which basically runs the whole program
  }
  public static void para () { // Method is void so we don't have to return anything
   Scanner input = new Scanner(System.in); // Declares and intializes the scanner "input"
   String adj = ""; // Declares all the variables I'm going to use
   String adj2 = "";
   String adj3 = "";
   String sub = "";
   String vrb = "";
   String obj = "";
   String actSent = "";
   String finSent = "";
   boolean status = true;

   while (status) { // While loop to create multiple possible thesis sentences
   adj = adjGenerator(adj); // Calls the adjGenerator and sends a copy of the variable adj to it
   adj2 = adjGenerator(adj2);
   adj3 = adjGenerator(adj3);
   sub = subGenerator(sub);
   vrb =  vrbGenerator(vrb);
   obj = objGenerator(obj);

   System.out.print("The " + adj + " " + adj2 + " " + sub + " " + vrb + " the " + adj3 + " " + obj + "."); //Prints out the returned variables for each generator method
   System.out.print ("(Would you like another sentence? Enter yes or no)");
   String resp = input.nextLine(); // Stores the user's response as a string "resp"

   if (resp.equalsIgnoreCase("yes")) { // If resp equals "yes" regardless of capitalization...
     status = true; // The loop repeats
   }
   else if (resp.equalsIgnoreCase("no")) { // If resp equals "no"...
    Random generator = new Random(); // We create another random generator
    int rand = generator.nextInt(5); // We set the value of the int rand to a random integer between 0 and 4
    for (int i = 0; i <= rand; i++) { // This for loop runs as long as i is less than or equal to rand
      actSent = aSentence(sub); // For each loop we call the action sentence method and send a copy of the first subject to it
      System.out.println(actSent); // We print out the result of this method as the string "actSent"
      }
     finSent = conSent(sub, adj); // Fianlly, we send the first subject/adjective to the conSent method and print out the result as the string finSent
     System.out.println(finSent);
     status = false; // Closes the hile while
   }
   else { // If anything else is entered...
     System.out.println ("Please enter valid input.");
     status = false; // The while loop is closed
   }
  

   }

 }

    public static String adjGenerator (String adjective) { //adjGenerator method, takes a copy of the String adj and sets it to String adjective
    Random generator = new Random(); // Creates a new random generator
    int rand1 = generator.nextInt(10); // Sets it to a number between 0 and 9 called rand1
     switch (rand1) {
       case 0: // If rand1 is 0
         adjective = "Ornery"; // Adjective is set to ornery
         break; // Breaks the case to prevent leakage
       case 1: 
         adjective= "Smelly";
         break;
       case 2:
         adjective = "Repulsive";
         break;
       case 3:
         adjective = "God-like";
         break;
       case 4:
         adjective = "Omnipotent";
         break;
       case 5:
         adjective = "Rambunctious";
         break;
       case 6:
         adjective = "Foreign";
         break;
       case 7:
         adjective = "Destitute";
         break;
       case 8:
         adjective = "Bloodlusted";
         break;
       case 9:
         adjective = "Trilingual";
         break;
     }

     return adjective; // Returns the variable adjective to the method that called it

   }


   public static String subGenerator (String subject) {
     Random generator = new Random();
     int rand2 = generator.nextInt(10);
     switch (rand2) {
       case 0:
         subject = "Dog";
         break;
       case 1: 
         subject  = "Cat";
         break;
       case 2:
         subject = "Frog";
         break;
       case 3:
         subject = "Man";
         break;
       case 4:
         subject = "Woman";
         break;
       case 5:
         subject = "President";
         break;
       case 6:
         subject = "Alien";
         break;
       case 7:
         subject = "Professor";
         break;
       case 8:
         subject = "Teaching Assistant";
         break;
       case 9:
         subject = "Robot";
         break;
     }

     return subject;

   }

   public static String vrbGenerator (String verb) {
     Random generator = new Random();
     int rand3 = generator.nextInt(10);
     switch (rand3) {
       case 0:
         verb = "kicked";
         break;
       case 1: 
         verb  = "slapped";
         break;
       case 2:
         verb = "ate";
         break;
       case 3:
         verb = "passed";
         break;
       case 4:
         verb = "punched";
         break;
       case 5:
         verb = "attacked";
         break;
       case 6:
         verb = "revered";
         break;
       case 7:
         verb = "controlled";
         break;
       case 8:
         verb = "loved";
         break;
       case 9:
         verb = "cherised";
         break;
     }

     return verb;

   }

   public static String objGenerator (String object) {
     Random generator = new Random();
     int rand4 = generator.nextInt(10);
     switch (rand4) {
       case 0:
         object = "Dog";
         break;
       case 1: 
         object = "Cat";
         break;
       case 2:
         object = "Frog";
         break;
       case 3:
         object = "Man";
         break;
       case 4:
         object = "Woman";
         break;
       case 5:
         object = "President";
         break;
       case 6:
         object = "Alien";
         break;
       case 7:
         object = "Professor";
         break;
       case 8:
         object = "Teaching Assistant";
         break;
       case 9:
         object = "Robot";
         break;
     }

     return object;

   }

   public static String aSentence (String aSub) { // Action sentence method. Accepts the string sub from para and copies it to aSub. Returns a string
     Random generator = new Random(); // Creating another random generator
     int rand5 = generator.nextInt(2); // Sets the integer rand5 to a random number between 0 and 1
     if (rand5 == 1) { // If rand5 is 1
       aSub = "it"; // The string aSub(The first subject) is converted to "it"
     }
     else { // Otherwise...
       aSub= "the " + aSub; // "The" is added in front of it
     }
     String vrb2 = ""; //Declares more variables for the actions sentences
     String adj4 = "";
     String obj2 = "";
     
     vrb2= vrbGenerator(vrb2); // Calls the generator methods to generate strings for the variables. Sends a copy of the variable to the generator
     adj4 = adjGenerator(adj4);
     obj2 = objGenerator(obj2);

     String moreSent = "Then, " + aSub + " " + vrb2 + " the " + adj4 + " " + obj2 + "."; // Declares and intializes string "moreSent" with the generated words

     return moreSent; // Returns the string "moreSent" to the method that called it
   }

   public static String conSent(String fSub, String fAdj) { // Conclusion sentence method. Returns a string and accepts copies of the first sub/adj, "sub" and "adj"
     String vrb3 = ""; // Declares more variables for the concluding sentence
     String adj5 = "";
     String obj3 = "";
     
     vrb3= vrbGenerator(vrb3); // Calls the generator methods to generate strings for the variable. Sends a copy of each variable to the generator
     adj5 = adjGenerator(adj5);
     obj3 = objGenerator(obj3);

     String fSent = "I just can't believe that " + fAdj + " " + fSub + " really " + vrb3 + " that " + adj5 + " " + obj3 + "!"; // Declares and intializes string "fSent"

     return fSent; // Returns fSent to the method that called it
   }




 }
