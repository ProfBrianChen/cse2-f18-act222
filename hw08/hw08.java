/* Andrew Taulane
CSE002 - Homework #8*/
import java.util.*;
public class hw08{ 
  // Every java program needs a main method
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); // Declares and intializes a new scanner called scan
    String[] suitNames={"C","H","S","D"};  // Decalares and intializes the arrays we will use. cards and hand are not alloted here because they will be assigned values later 
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int index = 51; // Sets the int index to 51 and again to 1
    int again = 1;
    
    for (int i = 0; i < 52; i++){ // Runs 52 times, and assigns the array cards a letter from suitNames and a "number" (really a string) from rankNames
      cards[i]=rankNames[i%13]+suitNames[i/13];  
    } 

    printArray(cards); // Sends the array cards to the void method printArray
    System.out.println(""); // Space for aesthetics
    System.out.println("Shuffled"); 
    shuffle(cards); // Sends the array cards to the void method shuffle
    printArray(cards); //Sends the shuffled array cards to the printArray method. We can modify strings in arrays so we don't have to do anything besides state the method and array we're passing
    
    System.out.println("");
    System.out.print("How big should the hand be?");
    int numCards = scan.nextInt(); // Assigns the int numCards to the next value of scanner scan 

    while (again == 1){ // While the int again is 1...
      if (index-numCards < 0) { // This if statement will reset the index to 51 and generate a new deck if the cards in the current deck run out
        index = 51;
        for (int i = 0; i < 52; i++){
          cards[i]=rankNames[i%13]+suitNames[i/13];
        }
        shuffle(cards);
      }
      hand = getHand(cards, index, numCards); // Assigns the array hand to the returned array from the getHand method
      System.out.println(""); 
      printArray(hand); // Sends the array hand to the printArray method
      index = index - numCards; // Decrease the int index by the int numCards
      System.out.println("");
      System.out.print("Enter a 1 if you want another hand drawn"); // Asks use to enter 1 to continue the loop or anything else to end it
      again = scan.nextInt(); 
  }
    
 } // Closes main method

 public static void printArray (String[] cards) { // Void method printArray that accepts the array cards
   for (int i = 0; i < cards.length; i++) { // Runs as long as the length of the array cards
     System.out.print(cards[i]+" "); // Prints out each element of the array
   }
 }
 public static void shuffle(String[] cards) { // Void  method shuffle that accepts the array cards 
   String j = ""; // Place holder string j
   for (int i = 0; i < 60; i++) { // The deck will be shuffled 60 times
     Random rand = new Random(); // Creates a new random called rand
     int random = rand.nextInt(52); // Sets the int random to a random number between 0 and 51
     j = cards[0]; // The value of the 0th index of the array cards is stored as string j
     cards[0] = cards[random]; // Switches the 0th index with a random index
     cards[random] = j; // Assigns the value of this random index to the value of j, shuffling the cards
   }
 }
 public static String[] getHand(String[] cards, int index, int numCards) { // getHand method that returns a string array and accepts the string array cards, and two ints
   String[] k = new String[numCards]; // Creates new string array k. Sets its length to the numCards
   for (int i = 0; i < numCards; i++) { // For loop that repeats as long as i is less than numCards
     k[i] = cards[index]; // Sets the value of k to the ith index to the value of cards at index index
     index--; // Decements the value index
   }
   return k; // Returns the string array k
 }
} // Closes program