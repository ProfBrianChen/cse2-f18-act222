/* Andrew Taulane
CSE002 - Hw05v*/
// This program deals a user inputted number of card hands and tells the probability of four types of hand
import java.util.*;
class Hw05 {
  // Every java program needs a main method
 public static void main(String[] args) {
   Scanner input = new Scanner (System.in);
   int runs = 0; // Declare and intialize all my vars in the outer scrope
   int uruns = 0;
   int card1 = 0;
   int card2 = 0;
   int card3 = 0;
   int card4 = 0;
   int card5 = 0;
   int onep = 0;
   int twop = 0;
   int tkind = 0;
   int fkind = 0; 
   double p1 = 0.0;
   double p2 = 0.0;
   double p3 = 0.0;
   double p4 = 0.0;
   boolean status = true;
   boolean status2 = true;

   while (status) {
     System.out.print ("How many hands would you like to run?");
     status2 = input.hasNextInt(); // Checks if user has entered an int, if they did, the program runs
      if (status2) {
        uruns = input.nextInt();
        status = false;
      }
      else {
        input.next(); // Clears the string
        System.out.println ("Please enter an integer.");
        status = true;
      }
     } 

     while (runs < uruns) {
       for (int i = 0; i < 5; i++) {
         card1 = (int) ((Math.random() * 51) + 1); 
         card2 = (int) ((Math.random() * 51) + 1);
         card3 = (int) ((Math.random() * 51) + 1);
         card4 = (int) ((Math.random() * 51) + 1);
         card5 = (int) ((Math.random() * 51) + 1);

         if (card1 == card2 || card2 == card3) { 
           i--; // Reruns the loop if there are two of the same card
         }
         else if (card1 == card3 || card2 == card4) {
           i--;
         }
         else if (card1 == card4 || card2 == card5) {
           i--;
         }
         else if (card1 == card5 || card3 == card4) {
           i--;
         }
         else if (card3 == card5 || card4 == card5) {
           i--;
         }

         }
         card1 = card1 % 13; // Each mathing card in a deck has the same remainder when divided by 13
         card2 = card2 % 13;
         card3 = card3 % 13;
         card4 = card4 % 13;
         card5 = card5 % 13;
        
      
         runs++; // Signals to the while loop that a hand of cards has been successfully dealt

         if (card1 == card2 && card2 == card3 && card3 == card4) { // I spell out every possible case
           fkind++;
         }
         else if (card1 == card2 && card2 == card3 && card3 == card5) {
           fkind++;
         }
         else if (card1 == card2 && card2 == card4 && card4 == card5) {
           fkind++;
         }
         else if (card1 == card3 && card3 == card4 && card4 == card5) {
           fkind++;
         }
         else if (card2 == card3 && card3 == card4 && card4 == card5) {
           fkind++;
         }
         else if (card1 == card2 && card2 == card3) {
           tkind++;
         }
         else if (card1 == card2 && card2 == card4) {
           tkind++;
         }
         else if (card1 == card2 && card2 == card5) {
           tkind++;
         }
         else if (card1 == card3 && card3 == card4) {
           tkind++;
         }
         else if (card1 == card3 && card3 == card5) {
           tkind++;
         }
         else if (card1 == card4 && card4 == card5) {
           tkind++;
         }
         else if (card1 == card2 && card3 == card4) {
           twop++;
         }
         else if (card1 == card2 && card3 == card5) {
           twop++;
         }
         else if (card1 == card2 && card4 == card5) {
           twop++;
         }
         else if (card1 == card3 && card2 == card4) {
           twop++;
         }
         else if (card1 == card3 && card2 == card5) {
           twop++;
         }
         else if (card1 == card3 && card4 == card5) {
           twop++;
         }
         else if (card1 == card4 && card2 == card3) {
           twop++;
         }
         else if (card1 == card4 && card2 == card5) {
           twop++;
         }
         else if (card1 == card4 && card3 == card5) {
           twop++;
         }
         else if (card1 == card5 && card2 == card3) {
           twop++;
         }
         else if (card1 == card5 && card2 == card4) {
           twop++;
         }
         else if (card1 == card5 && card3 == card4) {
           twop++;
         }
         else if (card1 == card2) {
           onep++;
         }
         else if (card1 == card3) {
           onep++;
         }
         else if (card1 == card4) {
           onep++;
         }
         else if (card1 == card5) {
           onep++;
         }
         else if (card2 == card3) {
           onep++;
         }
         else if (card2 == card4) {
           onep++;
         }
         else if (card2 == card5) {
           onep++;
         }
         else if (card3 == card4) {
           onep++;
         }
         else if (card3 == card5) {
           onep++;
         }
         else if (card4 == card5) {
           onep++;
         }


         p1 = (double) onep / (double) uruns; // Ensuring that there is 3 deciminal spaces in my probabilities
         p2 = (double) twop / (double) uruns;
         p3 = (double) tkind / (double) uruns;
         p4 = (double) fkind / (double) uruns;
         
         int a = (int) (p1 * 1000.0);
         int b = (int) (p2 * 1000.0);
         int c = (int) (p3 * 1000.0);
         int d = (int) (p4 * 1000.0);

         p1 = ((double) a / 1000.0);
         p2 = ((double) b / 1000.0);
         p3 = ((double) c / 1000.0);
         p4 = ((double) d / 1000.0);

         
         


       }
      
       System.out.println ("You generated " + uruns + " hands." );
       System.out.println ("You had " + onep + " one-pairs.");
       System.out.println ("You had " + twop + " two-pairs.");
       System.out.println ("You had " + tkind + " three-of-a-kinds.");
       System.out.println ("You had " + fkind + " four-of-a-kinds.");
       System.out.println ("The probability of a one-pair is " + p1);
       System.out.println ("The probability of a two-pair is " + p2);
       System.out.println ("The probability of a three-of-a-kind is " + p3);
       System.out.println ("The probability of a four-of-a-kind is " + p4);

     }
   }
