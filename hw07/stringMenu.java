import java.util.*;
class stringMenu {
  //Every java program neesd a main method
  public static void main(String[] args) {
    String input = ""; // Declares the variables I will use in this method
    String sampText = "";
    boolean status = true;
    sampText = sampleText(sampText); // Sends the string "sampText" to the sampleText method and returns the output as the variable sampText
    System.out.println ("You entered: " + sampText);
    while (status){ // While the boolean status is true...
      input = printMenu(input); // Prompt user for input via the printMenu method and stores their response as the string input
      if (input.equalsIgnoreCase("q")) { // If their response is "q", we quit the while loop
        System.out.println ("Quitting...");
        status = false; // Ends the loop
      }
      else if (input.equalsIgnoreCase("c")) { // If their response is "c"...
        int count = getNumOfNonWsCharacters(sampText); // Run the getNumOfNonWsCharacters method, passing the string sampText to it. Returns its output as int count
        System.out.println ("Number of non-whitespace characters: " + count);
        System.out.println(""); // Adds a blank line for readability
        status = true; // Loop continues until user hits quit
      }
      else if (input.equalsIgnoreCase("w")) { // If response is "w"
        int count = getNumOfWords(sampText); // Passes sampText to getNumOfWords method. Output returned as int count
        System.out.println ("Number of words: " + (count + 1)); // One added to end to account for the last word, which the method doesn't
        System.out.println("");
        status = true; // Loop continues
      }
      else if (input.equalsIgnoreCase("f")) { // If their response is "f"...
        Scanner input2 = new Scanner(System.in); // Declares a new scanner, input2
        System.out.print ("What word would you like to find?");
        String word = input2.nextLine(); // stores the user's input as String "word"
        int count = findText(word, sampText); // Passes Strings word and sampText to the findText method. Returns that output as int count
        System.out.println ("There are " + count + " instances of that word.");
        System.out.println ("");
        status = true; // Loop continues
      }
      else if (input.equalsIgnoreCase("r")) { // If their response is "r"...
        sampText = replaceExclamation(sampText); // Passes a copy of sampText to replaceExclamation method, returns output as new sampText
        System.out.println ("Edited Text: " + sampText); // Displays edited sampText
        System.out.println ("");
        status = true; // Loop continues
      }
      else if (input.equalsIgnoreCase("s")) { // If their response is "r"...
        sampText = shortenSpaces(sampText); // Passes a copy of sampText to shortenSpaces method, returns output as new sampText
        System.out.println ("Edited text " + sampText); // Displays edited sampText
        System.out.println (""); 
        status = true; // Loop continues
      }
      else {
        System.out.println ("Enter a valid option. Here's the menu again:"); // Accounts for user not hitting a valid menu option
        System.out.println ("");
        status = true; // And so the loop continues
      }
    }

  }
  public static String sampleText(String text) { //Returns a string and accepts copy of sampText, called string text
    Scanner input = new Scanner(System.in); // Declares a new scanner
    System.out.print ("Enter sample text: "); 
    text = input.nextLine(); // Stores user's next text as String text
    return text; // Returns string text to where it was called
  }
  public static String printMenu(String response) {
    Scanner input = new Scanner(System.in);
    System.out.println ("MENU:");
    System.out.println ("c - Number non-whitespace characters");
    System.out.println ("w - Number of words");
    System.out.println ("f - Find in text");
    System.out.println ("r - Replace all !'s");
    System.out.println ("s - Shorten all spaces");
    System.out.println ("q - quit");
    System.out.println ("");
    System.out.print ("Choose an option: ");

    response = input.nextLine(); // Strores the string response 
    return response; // Returns the string response to where to it was called
  }
  public static int getNumOfNonWsCharacters(String text) { // Accepts a string, returns an int
    int counter = 0;
    for (int i = 0; i < text.length(); i++) { // As long as int i is less than the length of the string text...
      if (text.charAt(i) != ' ') {  // If the character at i is not a blank space
        counter++; // Increment the counter by 1
      } // Increment i by 1 each loop
    }
    return counter; // Returns the int counter
  }
  public static int getNumOfWords (String text) { // String parameter, int return
    int counter = 0;
    for (int i = 0; i < text.length(); i++) { // As long as int i is less than the length of string text...
      if (text.charAt(i) == ' ' && text.charAt(i-1) != ' ') { // If the text at character i blank and the character before it is not blank
        counter++; // Increment the word counter by 1
      }
    }
    return counter; // Return the counter to where it was called
  }
  public static int findText (String search, String text) { // Two string parameters, int return
    int counter = 0;
    counter = (text.split(search, -1).length) - 1; // Uses split method to split the string text into words and checks if the string search is one of those words
    return counter; // Returns counter to where it was called
  }
  public static String replaceExclamation (String text) { // String parameter, String return
    String rtext = text.replace('!','.'); // If the char at i is an exclamation point, replace it with a period via the replace method
    return rtext; // Returns the edited string
  }
  public static String shortenSpaces (String text) { // String parameter, string return
    String stext = text.replace("  ", " "); // Replaces each double space with a single space via the replace method
    return stext; // Returns the edited string 
  }

}